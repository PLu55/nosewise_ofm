#!/usr/bin/python3
# Version 4.5.0

try:
    from collections.abc import Iterable
except ImportError: # python 3.5
    from collections import Iterable

import os
import time
import json
import asyncio

from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.dispatcher import Dispatcher
from pythonosc.udp_client import UDPClient
from pythonosc.osc_message_builder import OscMessageBuilder

verbose = True

def vprint(str):
    print('{:.9f} |  {}'.format(time.time(), str))

class MyUDPClient(UDPClient):
    """Simple OSC client that automatically builds :class:`OscMessage` from arguments"""

    def send_message(self, address, value, peer=None):
        """Build :class:`OscMessage` from arguments and send to server
        Args:
            address: OSC address the message shall go to
            value: One or more arguments to be added to the message
        """
        builder = OscMessageBuilder(address=address)
        if value is None:
            values = []
        elif not isinstance(value, Iterable) or isinstance(value, (str, bytes)):
            values = [value]
        else:
            values = value
        for val in values:
            builder.add_arg(val)
        msg = builder.build()
        self.send(msg, peer)

    def send(self, content, peer=None) -> None:
        """Sends an :class:`OscMessage` or :class:`OscBundle` via UDP
        Args:
            content: Message or bundle to be sent
        """
        if peer is None:
            self._sock.sendto(content.dgram, (self._address, self._port))
        else:
            self._sock.sendto(content.dgram, peer)
            
class OscServer(object):
    def __init__(self, ip='0.0.0.0', port=8000,
                 verbose=False, leds=False, simulate=False,
                 nofan=False):
        if simulate:
            from dummy_controller import OFMController
            verbose = True
        else:
            from controller import OFMController
        self.target = OFMController(verbose=verbose, leds=leds, fan=not nofan)
        self.ip = ip
        self.port = port
        self.verbose = verbose
        self.show_leds = leds
        
        self.button_cnt = 0
        self.button_stage = 0
        self.fan_state = 'normal'
        self.setup()
        self.target.leds = leds

    def start(self):
        asyncio.run(self.init_loop())

    def setup(self):
        self.dispatcher = Dispatcher()
        self.dispatcher.set_default_handler(self.default_handler)
        self.dispatcher.map('/odor/emit/*', self.emit_handler)
        self.dispatcher.map('/odour/emit/*', self.emit_handler)
        self.dispatcher.map('/odor/ratio/*', self.ratio_handler)
        self.dispatcher.map('/odour/ratio/*', self.ratio_handler)
        self.dispatcher.map('/fan/speedX', self.speed_handler)
        self.dispatcher.map('/fan_speed', self.speed_handler)
        self.dispatcher.map('/leds', self.leds_handler)
        self.dispatcher.map('/sequence', self.sequence_handler)
        self.dispatcher.map('/ping', self.ping_handler, needs_reply_address=True)
        self.client = MyUDPClient('127.0.0.1', 8000)

    def default_handler(self, address, *args):
        if verbose:
            vprint(f'received unknown message {address} {args}')
        print(f'Warning: received unknown message {address} {args}')
                            
    def emit_handler(self, address, *args):
        which = int(address.rpartition('/')[2])
        state = int(args[0])
        if verbose:
            vprint(f'received message {address}, {which}, {state}')
        self.target.odor_emit(which, state)

    def ratio_handler(self, address, *args):
        which = int(address.rpartition('/')[2])
        value = float(args[0])
        if verbose:
            vprint(f'received message {address}, {which}, {value}')
        self.target.set_odor_ratio(which, value)
            
    def speed_handler(self, address, *args):
        value = float(args[0])
        if verbose:
            vprint(f'received message {address}, {value}')
        self.target.set_fan_speed(speed=value)
            
    def leds_handler(self, address, *args):
        state = int(args[0])
        if verbose:
            vprint(f'received message {address}, {state}')

    def sequence_handler(self, address, *args):
        arg = args[0]
        if verbose:
            vprint(f'received message {address}, {arg}')
        jseq = json.loads(arg)
        seq={}
        for k, v in jseq.items():
            seq[float(k)]=v
        self.target.set_opening_sequence(seq)

    def ping_handler(self, client, address, *args):
        if verbose:
            vprint(f'received message {address}, {arg}, {client}')
        self.client.send_message(address, args, client)

    async def loop(self):
        while True:
            self.update_button_state()
            await asyncio.sleep(0.2)

    async def init_loop(self):
        server = AsyncIOOSCUDPServer((self.ip, self.port),
                                     self.dispatcher,
                                     asyncio.get_event_loop())
        transport, protocol = await server.create_serve_endpoint()
        await self.loop()
        transport.close()

    def shut_down(self):
        self.target.shut_down()

    def toggle_leds(self):
        self.show_leds = not self.show_leds
        self.target.leds = self.show_leds
        if not self.show_leds:
            self.target._set_all_leds(0.0)

    def toggle_fan_speed(self, state=None):
        if state is None:
            state = self.fan_state
        elif state == 'normal':
            state = 'forced'
        elif state == 'forced':
            state = 'normal'
        if state == 'forced':
            if self.verbose:
                vprint('toggle_fan_speed: forced')
            self.target.set_fan_speed(force=True)
            self.fan_state = 'normal'
        elif state == 'normal':
            if self.verbose:
                vprint('toggle_fan_speed: normal')
            self.target.set_fan_speed(speed=1.0, force=True)
            self.fan_state = 'forced'

    def update_button_state(self):
        state = self.target.get_button()
        if state and not self.button_state:
            if self.verbose:
                vprint(f'button pressed, toggle fan speed.')
            self.toggle_fan_speed()
            self.button_cnt = 0
            self.button_stage = 1
        elif state:
            self.button_cnt += 1
            if self.button_cnt >= 15 and self.button_stage == 1:
                if self.verbose:
                    vprint(f'button pressed, toggle leds')
                self.toggle_leds()
                self.toggle_fan_speed(state='normal')
                self.button_stage = 2
            if self.button_cnt >= 30:
                if self.verbose:
                    vprint(f'button pressed, shut down')
                server.shut_down()
                self.target._set_all_leds(1.0)
                time.sleep(0.2)
                self.target._set_all_leds(0.0)
                time.sleep(0.2)
                self.target._set_all_leds(1.0)
                time.sleep(0.2)
                self.target._set_all_leds(0.0)
                if os.geteuid() == 0:
                    call('poweroff')
                sys.exit(0)
        else:
            self.button_cnt = 0
            self.button_stage = 0

        self.button_state = state

if __name__ == '__main__':
    import sys
    import signal
    import logging
    import logging.handlers
    import traceback
    import argparse

    server = None
    parser = argparse.ArgumentParser(description='OFM Server using OSC.')
    parser.add_argument('-v', '--verbose', help='increase output verbosity',
                        action='store_true')
    parser.add_argument('-s', '--simulate', help='simulate action, no physical response',
                        action='store_true')
    parser.add_argument('-l', '--leds', help='turn leds active',
                        action='store_true')

    parser.add_argument("-p", "--port", type=int, default=8000,
                    help="incoming port")
    parser.add_argument('-f', '--nofan',
                        help='no auto start of the fan',
                        action='store_true')

    args = parser.parse_args()

    if args.verbose:
        print('Verbose on\n')
        print(f'Effective user id: {os.geteuid()}')
    if args.verbose and args.leds:
        print('LEDS are active\n')

    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.INFO)
    handler = logging.handlers.SysLogHandler(address = '/dev/log')
    my_logger.addHandler(handler)
    my_logger.info('OFMServer: Starting!')

    def sigint_handler(sig, frame):
        print('Recieved ^C, shuting down!')
        my_logger.info('OFMServer: Info:  Recieved ^C, shuting down!')
        if not server is None:
            server.target.shut_down(reset=False)
        sys.exit(0)
        
    signal.signal(signal.SIGINT, sigint_handler)

    try:
        server = OscServer(simulate=args.simulate,
                           port=args.port,
                           verbose=args.verbose,
                           leds=args.leds,
                           nofan=args.nofan)

    except Exception as e:
        my_logger.error(f'OFMServer: Error:  {e}')
        print(str(e))
        traceback.print_exc()
        sys.exit()

    try:
        server.start()

    except Exception as e:
        my_logger.error(f'OFMServer: Error:  {e}')
        print(str(e))
        traceback.print_exc()
        server.shut_down()
        sys.exit()
