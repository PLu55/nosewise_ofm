#!/usr/bin/python3
# Version 4.5.0

import sys

c = None

def sigint_handler(sig, frame):
    print('Recieved ^C, shuting down!')
    if not c is None:
        c.shutDown(reset=False)
    sys.exit(0)

if __name__ == '__main__':
    import time
    
    import signal
    import argparse

    signal.signal(signal.SIGINT, sigint_handler)

    parser = argparse.ArgumentParser(description='OFM servo adjustment utility.')
    #parser.add_argument('ratio', type=float,
    #                    help='how mush to open the servo(s)')
    parser.add_argument('ratio', type=float, nargs='?',
                    help='ratio to open a valve, [0-1]')

    parser.add_argument('-n', '--valve', type=int, choices=[1,2,3,4],
                        help='which servo to set')
    parser.add_argument('-v', '--verbose',
                        help='turn verbose mode on',
                        action='store_true')
    parser.add_argument('-s', '--simulate',
                        help='turn simulation mode on, no physical response',
                        action='store_true')
    parser.add_argument('-c', '--calibrated',
                        help='turn calibrated mode on',
                        action='store_true')

    args = parser.parse_args()

    if not args.ratio is None and (args.ratio < 0.0 or args.ratio > 1.0):
        parser.error('Illegal ratio, must be [0-1]')
    
    if args.simulate:
        from dummy_controller import OFMController
    else:
        from controller import OFMController

    c = OFMController(reset=False)
    if not args.calibrated:
        c.raw_servos()

    if args.valve is None and args.ratio is None:
        pos = 0
        val = 0.5
        if args.verbose:
            print(f'Setting all servos to 0.5')
        for i in range(1,5):
            c._set_servo_position(i, val)
            time.sleep(1)
            if args.verbose:
                print('Release servos.')
            c._servo_release(i)
    elif args.valve is None:
        arg = float(args.ratio)
        if args.verbose:
            print(f'Set all servos to {arg}')
        for i in range(1,5):
            c._set_servo_position(i, arg)
            time.sleep(1)
            if args.verbose:
                print('Release servo.')
            c._servo_release(i)

    elif args.ratio is None:
        which = int(args.valve)
        pos = 0.5
        if args.verbose:
            print(f'Set all servo {which} to {pos}')
        c._set_servo_position(which, pos)
        time.sleep(1)
        if args.verbose:
            print('Release servo.')
        c._servo_release(which)
    else:
        which = int(args.valve)
        pos = args.ratio
        if args.verbose:
            print(f'Set all servo {which} to {pos}')
        c._set_servo_position(which, pos)
        time.sleep(1)
        if args.verbose:
            print('Release servo.')
        c._servo_release(which)

    c.shut_down(reset=False)
    if args.verbose:
        print('Exit!')
    sys.exit(0)
