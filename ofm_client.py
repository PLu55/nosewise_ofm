#!/usr/bin/python3
# Version 4.5.1

"""Client for the Nosewise olfactometer.
"""

import time
import pythonosc as osc
from pythonosc import udp_client
import json

class OFM_client(udp_client.SimpleUDPClient):
    
    def __init__(self, ip='192.168.1.1', port=8000, verbose=True):
        self.verbose = verbose
        udp_client.SimpleUDPClient.__init__(self, ip, port)
        
    def fan_speed(self, speed):
        if self.verbose: print(f'Set the speed of the fan to: {speed}') 
        self.send_message('/fan_speed', speed)
        
    def ratio(self, which, val):
        if self.verbose: print(f'Set the ratio of channel: {which} to {val}') 
        self.send_message(f'/odor/ratio/{which}', val)

    def emit_odor(self, which, state): 
        if self.verbose:
            print(f'Set valve of channel: {which} to state: {state}') 
        self.send_message(f'/odor/emit/{which}', state)

    def set_opening_seq(self, oseq):
        jstr = json.dumps(oseq)
        self.send_message('/sequence', jstr)

    def poweroff(self):
        self.send_message('/poweroff', 0)
        
    def leds(self, state):
        self.send_message('/leds', state)

    def ping(self):
        self.send_message('/ping', 0)

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='OFM test client.')

    parser.add_argument("-a", "--host", type=str, default="127.0.0.1",
                        help="IP-address of the olfactometer")
    parser.add_argument("-p", "--port", type=int, default=8000,
                        help="incoming port")
    parser.add_argument('-s', '--servo_setting',
                        help='set all servos to 0.5',
                        action='store_true')

    args = parser.parse_args()

    print(f'Connecting to: {args.host}:{args.port}')
    client = OFM_client(ip=args.host, port=args.port)

    client.ping()

    if args.servo_setting:
        for i in range(1, 5):
            client.ratio(i, 0.5)
            client.emit_odor(i, 1)

    else:
        print('Testing fan related message.')
        print('   ... setting the speed of the fan to full speed.')
        client.fan_speed(1.0)
        time.sleep(2.0)
        print('   ... to half speed.')
        client.fan_speed(0.5)
        time.sleep(2.0)
        print('   ... turn the fan off.')
        client.fan_speed(0.0)

        print('\nTesting valve related messages.')
        for i in range(1, 5):
            client.ratio(i, 0.5)
            client.ratio(i, 1.0)
            client.emit_odor(i, 1)
            time.sleep(1.0)
            client.emit_odor(i, 0)
            time.sleep(1.0)
        
