# Version 4.5.0
import time

def vprint(str):
    print('{:.9f} |  {}'.format(time.time(), str))

class OFMController(object):
    """This class is for testing only.
    """
    def __init__(self, verbose=True, simulate=False, leds=False,
                 reset=True, startup_flash=True, led_level=0.25):
        self.verbose = verbose
        self.simulate = simulate
        self.leds = leds
        self.reset = reset
        self.startup_flash = startup_flash
        self.led_level = led_level

    def odor_emit(self, which, state):
       if self.verbose:
            vprint(f'OFMController.odor_emit({which}, {state})')

    def set_odor_ratio(self, which, value):
        if self.verbose:
            vprint(f'OFMController.set_odor_ratio({which},{value})')
   
    def set_fan_speed(self, speed=None, force=False, raw=False):
         if self.verbose:
            vprint(f'OFMController.set_fan_speed(speed={speed}, force={force}, raw={raw})')

    def set_opening_sequence(self, sequence):
       if self.verbose:
            vprint(f'OFMController.set_opening_sequence({sequence})')

    def raw_servos(self):
        if self.verbose:
            vprint(f'OFMController.raw_servos()')

    def shut_down(self, reset=False):
        if self.verbose:
            vprint(f'OFMController.shut_down(reset={reset})')        
                
    def _set_servo_position(self, which, pos):
        if self.verbose:
            vprint(f'OFMController._set_servo_position({which}, {pos})')
            
    def _servo_release(self, which):
        if self.verbose:
            vprint(f'OFMController._servo_release({which})')
            
