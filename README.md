# nosewise_ofm v.4.6.0

This repository contains the software for the Nosewise handheld
olfactometer (NOHO).

This document describes how to build the NOHO system software from
scratch. If you do not have a particular reason to build the software
your self the recommendation is to download a [binary
image](https://osf.io/bpf6s/download) of the software and put it on a
SD-card.

This description assumes a Linux (Ubuntu 19.10) based host
system. Instructions can be found for other systems online.

The Nosewise handheld olfactometer is controlled by an onboard
Raspberry Pi Zero W single chip computer. All the software is written
in Python and is compatible with Python 3.7.

## How to build the NOHO system from scratch

### What operating system to use?

The operating system to use for the Raspberry Pi (RPi) is the Raspberry
Pi OS Lite. There are potential problems with compressing the system
image file if using another system then Raspberry Pi OS. Do **not**
install it via the NOOBS system.

### How to copy a system image to an SD-card (LINUX)

Open a terminal window on the host system (your computer) and move to
a suitable directory.

```
> cd <suitable directory>
```

Download Raspberry Pi OS Lite (Debian Buster as Feb 2020) from
https://www.raspberrypi.org/downloads/raspberry-pi-os/

```
> wget https://downloads.raspberrypi.org/raspios_lite_armhf_latest
```

The name of the downloaded file is ``raspios_lite_armhf_latest``.

Optionally check the checksum of the file, the correct key can be
found at the Raspberry Pi OS download page.

```
> echo "<replace with key> *raspios_lite_armhf_latest" | sha256sum -c -
```

Insert an SD-card in the card reader of your system, do not mount
it. Use fdisk to find out which device is the SD card:

```
> sudo  fdisk -l
```

Look for a line like this: ``Disk /dev/sdX`` where X is a letter, all
disks in the system has a similar entry but the SD-card is probably
found at the end of the listing. If there are any doubts about which
device it which, try to first run the fdisk command with the SD-card
unplugged then with the card inserted and look for the difference. It
is very important that the right device is selected.

**The following is a potentially dangerous operation!** The dd command can easily
corrupt your system if the wrong device is given as argument (the ``of``
option). Be careful!

Copy the compressed image file to an SD-card, replace <SD-card device>
with the current SD device:

```
> unzip -p raspios_lite_armhf_latest | sudo dd of=<SD-card device> bs=4M status=progress conv=fsync
```

### How to setup the RPi to access it via USB

To facilitate an easy system setup the Raspberry Pi (RPi) is setup so
it can be accessed via USB as an Ethernet device. The setup described
below follows this procedure:
https://desertbot.io/blog/ssh-into-pi-zero-over-usb

#### Install the operating system on a SD card

Copy a fresh Raspbian Lite system image file to the SD card as
described above.

Mount the SD card on the host system (an SD-card reader is needed) and
open the boot partition in a terminal window.
  
```
> cd <SD-card mount point>/boot
```
  
Create an empty file to enable SSH.

```
> sudo touch ssh
```

Edit config.txt, add a line with ``dtoverlay=dwc2`` at the end of the
file:

```
> sudo nano config.txt
```

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Edit cmdline.txt and add ``modules-load=dwc2,g_ether`` one space
behind rootwait:

```
> sudo nano cmdline.txt
```

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Flush and unmount the SD card:

```
> sync
> umount <SD-card mount point>
```

Take out the SD-card and insert it into the RPi.

#### Start the system and login

Connect a USB cable to the USB port (not the power port) of the RPi
and to a host computer (preferably running Linux, it's possible to use
other OS but that is not described here). The RPi should now boot up,
it will take a minute or so.

Prepare the host system to connect to the RPi via USB. In the IP
settings on the host (Ubuntu) in System Settings/Connections create a
new wired connection and in the IPv4 tab select method ``Shared to other
computers``.

Create a new SSH key if you don't have one.

```
> ssh-keygen -t rsa -b 4096 -C "your_email@domain.com"
```
The output will look something like this:

```
Enter file in which to save the key (/home/yourusername/.ssh/id_rsa):
```

Press Enter to accept the default file location and file name. 

You will then be asked to give a password, just press Enter if you
don't want to have a password.

Transfer your SSH key to the RPi, the default password is raspberry:

```
> ssh-copy-id pi@raspberrypi.local
```

Login using SSH, password should not be needed:
  
```
> ssh pi@raspberrypi.local
```
  
*This is absolutely necessary!* Change the password to something
suitable, you will be prompted for a new password.

```
> passwd
```
  
#### Update and install the software

Update the system:

```
> sudo apt-get update
> sudo apt-get upgrade
```
  
Install required software:

```
> sudo apt-get install python-smbus i2c-tools python-pip3 git
```

Activate I2C, select Interfacing Options and then I2C enable, (see: [Adafruit's Configuring I2C](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c)):

```
> sudo raspi-config
```

Reboot:

```
> sudo reboot
```

Login again using SSH, password should not be needed now:
  
```
> ssh pi@raspberrypi.local
```

Check IC2 connection to Adafruit PWM, you should
see 40 and 70 in the output if everything is OK:

```
> i2cdetect -y 1
```

#### Change the hostname

Edit /etc/hostname, replace the content of the file with
``nosewise_00`` to the file then type ``[Ctrl-o][Ctrl-x]`` to save the
file and exit Nano:

```
> sudo nano /etc/hostname
```

Edit /etc/hosts, change the line that starts with ``127.0.1.1`` to
``127.0.1.1 nosewise_00`` then type ``[Ctrl-o][Ctrl-x]`` to save the
file and exit Nano.


```
> sudo nano /etc/hosts
```

#### Setup the RPi as an access point

DHCP server used is udhcpd from the BusyBox. Download and install the
DHCP server:

```
> sudo apt install dnsmasq hostapd
```

Turn the new DHCP server off:

```
> sudo systemctl stop dnsmasq
> sudo systemctl stop hostapd
```

Configuring a static IP address:

```
> sudo nano /etc/dhcpcd.conf
```

Add the following lines at the end of the file:

```
interface wlan0
static ip_address=192.168.1.1/24
nohook wpa_supplicant
```

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Start the DHCP server:

```
> sudo service dhcpcd restart
```

#### Configuring the DHCP server (dnsmasq)

Copy the original settings file for safety then edit the original
file:

```
> sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
> sudo nano /etc/dnsmasq.conf
```
 Add the following lines to the file:
 
```
interface=wlan0
dhcp-range=192.168.1.2,192.168.1.20,255.255.255.0,24h
```

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Start the DHCP service:

```
> sudo systemctl start dnsmasq
```

#### Configure the access point software (hostapd)

Copy the original configuration file for safety then edit the original
file:

```
> sudo cp /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.orig
> sudo nano /etc/hostapd/hostapd.conf
```

Add the following lines:

```
interface=wlan0
driver=nl80211
ssid=Nosewise_00
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=raspberry
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```
Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano. 

For security reasons the wpa_passphrase just entered should be changed
when the system is cloned to a secret and more secure one.

Edit the access point settings:

```
> sudo nano /etc/default/hostapd
```
Replace the line starting with ``#DAEMON_CONF`` with 
``DAEMON_CONF="/etc/hostapd/hostapd.conf"``

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.


#### Start the hostapd

```
> sudo systemctl unmask hostapd
> sudo systemctl enable hostapd
> sudo systemctl start hostapd
```

Check the status:

```
> sudo systemctl status hostapd
> sudo systemctl status dnsmasq
```

#### Add routing and masquerade

Edit /etc/sysctl.conf and uncomment line ``net.ipv4.ip_forward=1``:
 
```
> sudo nano /etc/sysctl.conf
```

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Edit the firewall settings:

```
> sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
> sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
```

Edit the boot rules and add a line containing ``iptables-restore <
/etc/iptables.ipv4.nat`` just above the line with "exit 0":

```
> sudo nano /etc/rc.local
```

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Reboot the system:

```
> sudo reboot
```

Login again using SSH:
  
```
> ssh pi@nosewise_00.local
```

Check if the access point "Nosewise_00" is visible:

```
> nmcli dev wifi | grep Nosewise_00
```

#### Clone the Nosewise software

Clone the Nosewise software Git respository:

```
> git clone https://gitlab.com/PLu55/nosewise_ofm.git
```

Install python package for the Adafruit PWM/servo controller and OSC:

```
> sudo pip3 install -r nosewise_ofm/requirements.txt
```

Start the OSC server:

```
python3 nosewise_ofm/ofm_osc_server.py
```

#### Test the software and hardware

Connect the USB cable of the olfactometer, the USB connector marked
with S to a USB power supply or a USB power bank. 

On the host machine open a new shell, go to a suitable directory and
copy the ofm_client.py file. 

```
> cd <where you like the software to be stored>
> scp pi@nosewise_00.local:nosewise_ofm/ofm_client.py .
```

Run the ofm_client:

```
> python3 ofm_client.py -a pi@nosewise_00.local
```

If everything is OK then on the olfactometer the fan should start and
the valves should be activated in a sequence, the test client will
write out on the screen what it is doing.
  
#### Activate the software

Add an entry to rc.local to start the software automatically at boot:

```
> sudo nano /etc/rc.local
```

Add a new line just before ``exit 0`` with ``python3 /home/pi/nosewise_ofm/ofm_osc_server.py &``


Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit Nano.

Reboot:

```
> sudo reboot
```

## How to create a system image file from the SD-card

This procedure can also be used to make a safety image file of the
system, if this is the case then just save the image file without any
changes.

Before creating an image of the SD card that is intended to be
distributed the following should be done, this should be skipped if the
image file is made as a backup:

- reset password to raspberry
- rm ssh known hosts file
- change the hostname to nosewise_00 as described above in "Change the hostname"
- reset wifi password to raspberry
- change wifi ssid to Nosewise_00

```
> sudo passwd raspberry
> sudo rm .ssh/known_hosts
> sudo nano /etc/hostapd/hostapd.conf
```

Change the line that starts with ``ssid`` to 
``ssid=Nosewise_00``

Change the line that starts with ``wpa_passphrase`` to 
``wpa_passphrase=raspberry``

Type ``[Ctrl-o][Ctrl-x]`` to save the file and exit
Nano.

Power off the system:

```
> sudo poweroff
```

When the system has halted and the green led close to the USB
connectors is off then completely unplug the RPi and take out the
SD-card from the RPi.
 
## How to copy and shrink a system image

*This only works if the system is based on the Raspberry Pi OS, it
will fail if based on NOOBS.*

### How to copy the system on an SD card to an image file (LINUX)

Open a new terminal window on your host system.

Insert the SD-card in the card reader in the host computer. Use fdisk
to find out which device is the SD card:

```
> sudo  fdisk -l
```

**The following is a potentially dangerous operation!** The dd command can easily
corrupt your system if the wrong device is given as argument (the ``of``
option). Be careful!

Copy from SD-card to an image file, replace ``<SD-card device>`` with the
current SD device and the  ``<image filename>`` with the
name of the image file:

```
> sudo dd bs=4M status=progress if=<SD-card device> of=<image filename>
```

The simplest procedure is to use
[PiShrink](https://www.ostechnix.com/pishrink-make-raspberry-pi-images-smaller/). This
is a shell script that will perform the necessary operations.
 
Download PiShrink:
 
```
> wget https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh
```
 
 Make the file executable:
 
 ```
 > chmod +x pishrink.sh
 ```
 
 Shrink the image file, it will auto expand when booting the RPi for
 the first time:
 
 ```
 > sudo pishrink.sh image-file-original.img image-file-shrunken.img
 ```
 
 Compress the shrunken image-file:
 
 ```
 > zip image-file-shrunken.img.zip image-file-shrunken.img
 ```

/PLu 2020.07.01

### Links:

https://desertbot.io/blog/raspberry-pi-resource-guide
https://www.raspberrypi.org/downloads/raspbian/
https://github.com/attwad/python-osc
https://serverfault.com/questions/439128/dd-on-entire-disk-but-do-not-want-empty-portion
https://www.ostechnix.com/pishrink-make-raspberry-pi-images-smaller/
