# Version 4.4.0

import json
import six

class Parameters(object):
    """Store and restore settings in a .json file
    """
    def __init__(self,**kw):
        #for k, v in kw.iteritems():
        for k, v in six.iteritems(kw):
            setattr(self, k, v)
    
    def to_json(self):
        return json.dumps(self.__dict__, indent=4, sort_keys=True)

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)
    
    @classmethod
    def load(cls, filename):
        with open(filename, 'r') as f:
            s = f.read()
        return cls.from_json(s)
    
    def save(self, filename):
        with open(filename, 'w') as f:
            f.write(self.to_json())

if __name__ == '__main__':
    
    p1 = Parameters(par_1=3, par_2='olle', par_3=5.34)
    print('p1: {}'.format(p1.to_json()))
    p1.save('tst.json')
    p2 = Parameters.load('tst.json')
    print('p2: {}'.format(p2.to_json()))
