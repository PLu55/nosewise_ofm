# Version 4.5.0

from math import fabs
import os
import json
import time
import shutil
import threading
import heapq

from Adafruit_PCA9685 import PCA9685
import RPi.GPIO as GPIO

from param import Parameters

dir = os.path.dirname(os.path.realpath(__file__))

SETTING_FILE = os.path.join(dir, 'settings.json')

def vprint(str):
    print('{:.9f} |  {}'.format(time.time(), str))

class Worker(threading.Thread):
    """A worker handles a single valve. It runs in it's own thread and has
    a queue of tasks, A task has a time and a value. The time is when
    the task should happen and the value is just a number, if it's positive the
    valve is opened with that much, if zero itr's closed and when it's negative
    the servo is released, (turned off).
    """
    def __init__(self, id, owner, verbose=False):
        self.id = id
        self.owner = owner
        self.verbose = verbose

        #self.opening_seq = owner.p.opening_seq
        self.sleep = owner.p.worker_sleep
        self.state = 'closed'
        self.tasks = []
        self.lock = threading.Lock()
        self.running = True
        
        self.emit_level = 0.0 # just for development
        
        super(Worker, self).__init__()

    def run(self):
        """The loop where the worker is doing it's jobs in. 
        Do not call this, it's called when the thread starts.
        """
        try:
            while self.running:
                if not self.tasks == []:
                    t = self.tasks[0][0]
                    if t <= time.time():
                        val = heapq.heappop(self.tasks)[1]
                        if val < 0:
                            self.release_valve()
                        else:
                            self.set_valve(val)
                time.sleep(self.sleep)
        except Exception as e:
            vprint('Exceptiuon: {}'.format(e))

    def terminate(self):
        """Terminate the worker.
        """
        self.running = False

    def get_ratio(self):
        """Get the current ratio for this worker.
        """
        ratio = self.owner.get_odor_ratio(self.id)
        if self.verbose:
            vprint('Worker.get_ratio()->{}'.format(ratio))
        return ratio

    def select_opening_seq(self, ratio):
        """Returns the opening sequence closest to the given ratio.
        """
        key = False
        a = 1.0
        for r in self.owner.p.opening_seq.keys():
            d = fabs(ratio-float(r))
            if d < a: 
                key = r
                a = d
        if self.verbose:
            vprint('Worker[{}].select_opening_seq({})-> key:{}'
                   .format(self.id, ratio, key))
        return self.owner.p.opening_seq[key]
    
    def add_task(self, task):
        """Add a new task to the task queue.
        """
        with self.lock:
            if task == 'begin':
                if not self.state == 'opened':
                    if self.verbose:
                        vprint('Worker[{}].add_task(begin)'.format(self.id))
                    t0 = time.time()
                    self.tasks = []
                    ratio = self.get_ratio()
                    opening_seq = self.select_opening_seq(ratio)
                    self.set_valve(opening_seq[0][1]*ratio)
                    self.state = 'opened'
                    for t, v in opening_seq[1:]:
                        self.queue_task(t0+t, v*ratio)
                    if not t is None:
                        d = self.owner.p.servo_hold
                    else:
                        d = t + self.owner.p.servo_hold
                    #self.queue_task(d, -1) # add a release task
                else:
                    if self.verbose:
                        vprint('Worker[{}].add_task(skip)'.format(self.id))
            elif task == 'end':
                if self.verbose:
                    vprint('Worker[{}].add_task(end)'.format(self.id))
                self.tasks = []
                self.set_valve(0.0)
                self.state = 'closed'
                d = self.owner.p.servo_hold
                #self.queue_task(d, -1) # add a release task

            if self.verbose:
                vprint('Worker[{}].add_task(): exits.'.format(self.id))

    def reset_queue(self):
        """Remove all tasks from the queue of tasks.
        """
        self.tasks = []

    def queue_task(self, at, task):
        """Add a task to the task queue.
        """
        heapq.heappush(self.tasks, (at, task)) 

    def next_task(self):
        """Get the next task from the queue of tasks.
        """
        return heapq.heappop(self.tasks)

    def queue_empty(self):
        """Check is the queue of tasks is empty..
        """
        return self.tasks == []

    def set_valve(self, val):
        """Set the position of the valve.
        """
        if self.verbose:
            vprint('Worker[{}].set_valve({})'.format(self.id, val))
        self.owner._set_valve(self.id, val)
        
    def release_valve(self):
        """Release the servo cotrolling the valve.
        """
        if self.verbose:
            vprint('Worker[{}].release_valve({})'.format(self.id, val))
        self.owner._servo_release(self.id)

class PWMDrySwim(object):
    """This class is for testing only.
    """
    def set_pwm_freq(self, freq):
        vprint('PWMDrySwim.set_pwm_freq(%f)' % freq)
        
    def set_pwm(self, chan, p1, p2):
        vprint('PWMDrySwim.set_pwm(%d, %d, %d)' % (chan, p1, p2))

class OFMController(object):
    """Controlls the Noisewise Olfactometer.
    """
    def __init__(self, verbose=False, simulate=False, leds=False,
                 reset=True, startup_flash=True, led_level=0.25, fan=True):
        try:
            self.p = Parameters.load(SETTING_FILE)
        except IOError:
            self.factory_settings()
            
        if verbose:
            vprint('OFMController.p: {}'.format(self.p.to_json()))
        self.verbose = verbose
        self.simulate = simulate
        self.leds = leds
        self.startup_flash = startup_flash
        self.led_level = led_level
        if simulate:
            self.pwm = PWMDrySwim()
        else:
            self.pwm = PCA9685(self.p.pwm_addr)
        self.pwm.set_pwm_freq(self.p.pwm_freq)

        # Setup the GPIO pins
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.p.buttonpin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.p.signalpin, GPIO.OUT)
        GPIO.output(self.p.signalpin, 0)
        
        self.valve_state = [0, 0, 0, 0]
        self.odor_ratio = [1.0, 1.0, 1.0, 1.0]
        self.opening_seq = None
        self.fan_state = 0
        self.workers = []

        if reset:
            self.reset()
        # ???
        if fan:
            self.set_fan_speed(force=True)
        self.start_workers()
        if self.startup_flash:
            self.startup_led_flash();
            
    def set_opening_sequence(self, oseq):
        self.p.opening_seq = oseq
        self.store_settings()
        
    def factory_settings(self):
        p = Parameters(
            version = "4.3.0",
            name =  "nosewise??",
            worker_sleep = 0.001,
            pwm_addr = 64,
            pwm_freq = 50.00,
            signalpin = 21,
            buttonpin = 4,
            fanpin = 14,
            fanled = 5,
            fan_min_speed = 0.4,
            fan_default_speed = 0.8,
            fan_kick_time = 0.3,
            servopins = [4, 5, 6, 7],
            servo_hold = 0.3,
            servo_min = 200,
            servo_max = 40,
            ledpins = [8, 9, 10, 11, 12],
            valve_direction = [-1, 1, -1, 1],
            valve_open = [0.6, 0.4, 0.6, 0.4],
            valve_closed = [0.4, 0.6, 0.4, 0.6],
            opening_seq = {1.00:  [[0.0, 0.7],[0.2, 0.8],[0.45, 1.0]],
                           0.75 : [[0.0, 0.7],[0.2, 0.8],[0.45, 1.0]],
                           0.67 : [[0.0, 0.7],[0.2, 0.8],[0.45, 1.0]],
                           0.50 : [[0.0, 0.7],[0.2, 0.8],[0.45, 1.0]],
                           0.33 : [[0.0, 0.7],[0.2, 0.8],[0.45, 1.0]],
                           0.25 : [[0.0, 0.7],[0.2, 0.8],[0.45, 1.0]]})
        self.p = p
        self.store_settings()

    # TODO: limit the number of backup setting files
    def store_settings(self):
        if os.path.exists(SETTING_FILE):
            dir = os.path.dirname(SETTING_FILE)
            fn = os.path.basename(SETTING_FILE)
            i = 1
            while  True:
                dest = os.path.join(dir,fn+'.copy{}'.format(i))
                if not os.path.exists(dest):
                    shutil.copy2(SETTING_FILE, dest)
                    break
                i +=  1
        self.p.save(SETTING_FILE)
        
    def start_workers(self):
        for i in range(4):
            w = Worker(i+1, self, verbose=self.verbose)
            self.workers.append(w)
            w.start()

    def raw_servos(self):
        """Warning, this might harm the servos!

        The calibration of the servos are overrided and set to full range.
        Warning this might harm the servos if the servos are mechanically
        hindered to move to a given position, use with care. The purpose
        of this method is to be used to find calibration settings.
        """
        self.p.valve_open = [1.0, 0.0, 1.0, 0.0]
        self.p.valve_closed = [0.0, 1.0, 0.0, 1.0]

    def odor_emit(self, which, state):
        """Sets the odour emit state to the given state

        Arguments:
            which : channel to change the state of [1-4].
            state : is the state, , if 1 turn on if 0 turn off.
        """
        if state:
            GPIO.output(self.p.signalpin, 1)
            self._begin_emit(which)
        else:
            GPIO.output(self.p.signalpin, 0)
            self._end_emit(which)

    def get_odor_ratio(self, which):
        """Returns the ratio of the odour release, how strong odour will be emitted.

        Arguments:
            which : channel to get the emition ratio of [1-4].
        """
        return self.odor_ratio[which-1]
    
    def set_odor_ratio(self, which, ratio):
        """Sets the ratio of the odour release, how strong odour will be emitted.

        Arguments:
            which : channel to change the emition ratio of.
            ratio : the ratio to set, [0-1].
        """
        if self.verbose:
            vprint('OFMController.set_odor_ratio chan: %d val: %f' % (which, ratio))
        self.odor_ratio[which-1] = ratio

    def set_fan_speed(self, speed=None, force=False, raw=False):
        """Sets the speed of the fan as a ratio of the differens 
        between minimum speed and full speed.

        Arguments: 
            speed : if None then the speed is set to the default value if it's a
                    number then it's the normalized fan speed [0-1] where 0 turns
                    the fan off and 1 is full speed.
            raw : if False the fan speed is adjusted to a value between min speed
                  and full speed but 0 turns the fan off. If True the speed is not
                  adjusted, too low value will not start the fan.
        """
        pspeed = self.p.fan_default_speed

        if speed != pspeed or force:
            if speed is None:
                speed = pspeed
            if speed <= 0:
                if self.verbose:
                    vprint('OFMController.set_fan_speed. turn fan of, speed is zero')
                self.pwm.set_pwm(self.p.fanpin, 0, 4096)
                if self.leds:
                    self._set_led(self.p.fanled, 0)
            else:
                if self.fan_state == 0:
                    self.pwm.set_pwm(self.p.fanpin, 4096, 0)
                    time.sleep(self.p.fan_kick_time)
                if raw:
                    s = speed
                else:
                    s = speed * (1.0 - self.p.fan_min_speed) + self.p.fan_min_speed
                if s >= 1.0:
                    self.pwm.set_pwm(self.p.fanpin, 4096, 0)
                else:
                    self.pwm.set_pwm(self.p.fanpin, 0, int(4095 * s))
                if self.leds:
                    self._set_led(self.p.fanled, speed)
                if self.verbose:
                    vprint('OFMController.set_fan_speed nominal: %f adjusted speed: %f ' % (speed, s))
            self.fan_state = speed

    def shut_down(self, reset=True):
        """Turns the fan off and stop any ongoing emision and terminates all workers.

        Arguments:
            reset : if False only the workers are terminated.

        """
        for w in self.workers:
            w.terminate()
        self.workers = []
        if reset:
            self.set_fan_speed(1.0)
            self.reset()
            time.sleep(2)
        GPIO.cleanup()

    def reset(self):
        """Resets the device to initial state, then reads the parameter file
        and restores the values to that state. 
        """
        if self.verbose:
            vprint('OFMController.reset')
        for i, pin in enumerate(self.p.servopins):
            self._set_valve(i, 0.0)
        time.sleep(self.p.servo_hold)
        for i, pin in enumerate(self.p.servopins):
            self._servo_release(i)
        time.sleep(self.p.servo_hold)

    def _begin_emit(self, which):
        """Request the worker that controls the particular valve to open it.
        """
        ratio = self.odor_ratio[which-1]
        if self.verbose:
            vprint('OFMController._begin_emit({}, {})'.format(which, ratio))
        self.workers[which-1].add_task('begin')
        
    def _end_emit(self, which):
        """Request the worker that controls the particular valve to close it.
        """
        if self.verbose:
            vprint('OFMController._end_emit({})'.format(which))
        self.workers[which-1].add_task('end')
        time.sleep(self.p.servo_hold)
        #self._servo_release(which)

    def _set_valve(self, which, position):
        """Sets a valve to the given position.
        """
        if self.verbose:
            vprint('OFMController._set_valve({}, {})'.format(which, position))
        self.valve_state[which-1] = position
        pwm = (position * (self.p.valve_open[which-1]-self.p.valve_closed[which-1])
                + self.p.valve_closed[which-1])   
                   
        self._set_servo_position(which, pwm)
        if self.leds:
            self._set_led(which, position)
        #if self.p.servo_hold > 0:
        #    time.sleep(self.p.servo_hold)
        #    self._servo_release(which)

    def _set_servo_pulse(self, channel, pulse):
        """Calculate the servos pulse width.
        """
        pulse_length = 1000000    # 1,000,000 us per second
        pulse_length //= self.p.pwm_freq       # period
        pulse_length_bits = 4096     # 12 bits of resolution
        if self.verbose:
            vprint('{0}us per period'.format(pulse_length))
            vprint('{0}us per bit'.format(pulse_length_bits))
        pulse *= 1000
        pulse //= pulse_length_bits
        return pulse

    def _set_servo_position(self, which, position):
        """Sets the position of a servo. position is between [0-1].
        """
        if self.verbose:
            vprint('OFMController._set_servo_position({}, {})'.format(which, position))
        pulse = int(position * (self.p.servo_max - self.p.servo_min) + self.p.servo_min)
        self.pwm.set_pwm(self.p.servopins[which-1], 0, pulse)

    def _servo_release(self, which):
        """Release the given servo. 

        This method is not reliable, it occatonally set a random value. Seams to be a hardware
        problem.
        """
        if self.verbose:
            vprint('OFMController._servo_release({})'.format(which))
        self.pwm.set_pwm(self.p.servopins[which-1], 0, 4096)

    def _set_led(self, which, intensity):
        """Sets the intensity of the given led.

        Arguments:
            which : the number of the led to be set.
            intrensity : the intensity of the led [0-1].
        """
        intensity *= self.led_level
        if self.verbose:
            vprint('OFMController._set_led({}, {})'.format(which, intensity))
        pin = self.p.ledpins[which-1]
        intensity *= intensity
        pulse = int(4095 * intensity)
        if pulse > 0:
            self.pwm.set_pwm(pin, 0, pulse)
        else:
            self.pwm.set_pwm(pin, 0, 4096)

    def _set_all_leds(self, intensity):
        """Sets the intensity of all leads.
        """ 
        if self.verbose:
            vprint('OFMController._set_all_led({})'.format(intensity))
        intensity *= intensity
        for i in range(len(self.p.ledpins)):
            self._set_led(i+1, intensity)
        #pin = self.p.ledpins[which-1]

    def leds_reflect_state(self):
        #for i, v in enumerate(self.valve_state):
        #    self._set_led(i+1, v)
        #self._set_led(self.p.fanled, self.fan_state)
        None
        
    def get_button(self):
        """Returns the stae of the button.
        """
        return not GPIO.input(self.p.buttonpin)

    def set_signal_pin(self):
        """Activates the signal pin.

        This is used only for measuring of the olfactometer.
        """ 
        GPIO.output(self.p.signalpin)

    def startup_led_flash(self):
        """Flash the leds i sequence to indicate that initialization is finnished.
        """
        for i in range(5):
            self._set_led(i+1, 1.0)
            time.sleep(0.1)
            self._set_led(i+1, 0.0)
            time.sleep(0.1)

if __name__ == '__main__':

    c = OFMController(simulate=False)
    c.set_fan_speed(0.8)

    raw_input("press enter to quit...\n")
